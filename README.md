# Example app running on Kubernetes in DigitalOcean

A simple application to demonstrate and explore how to develop, deploy and run
an application in Kubernetes using GitOps principals.

## Provision from local clone

This will provision a number of resouces in digital ocean. This will include:

* A two node kubernetes cluster ($20/month or $0.030/hour)
* A managed databse ($15/month or $0.022/hour)
* A loadbalancer ($10/month or $0.015/hour)

Requirements:

- DigitalOcean account ([referal link $100 credit for 60 days]) and [API Token]
  ```bash
  export DIGITALOCEAN_TOKEN=<your digital ocean api token>
  ```
- Pulumi account ([signup](https://app.pulumi.com/signup))
  ```bash
  pulumi login  # follow instructions to log in via the browser
  ```

[referal link $100 credit for 60 days]: https://m.do.co/c/8fba3fc95fef
[API Token]: https://cloud.digitalocean.com/account/api/tokens/new


Then to provision the application:

```bash
cd deploy
pulumi up
```

_Note: There is a current race condition with the ingress controller and
ingress resources that might lead to this failing on the first run. Running it
again will then successfully create the remaining resources. See #6 for
details._

Once the loadbalancer is fully up and has an IP assigned to it. Get the IP and
replace it in each sercices Ingress resource at `deploy/kube/<service>.yml`:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
...
spec:
  rules:
    - host: todo-api.159.65.212.234.nip.io
```

Or replace the hostname with your own domain (managed externally until #2 is
done).

Once you are done experimenting with the instances they can be torn down to
avoid excessive cost with

```bash
cd deploy
pulumi destory
```
