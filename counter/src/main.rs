use rocket::{self, get, launch, routes, State};
use std::sync::atomic::{AtomicUsize, Ordering};

struct Counter(AtomicUsize);

#[get("/")]
fn hello(counter: State<Counter>) -> String {
    format!(
        "Hostname: {}\nCounter: {}\n",
        hostname::get().unwrap().to_str().unwrap(),
        counter.0.fetch_add(1, Ordering::SeqCst),
    )
}

#[launch]
fn rocket() -> rocket::Rocket {
    rocket::ignite()
        .manage(Counter(AtomicUsize::new(0)))
        .mount("/", routes![hello])
}
