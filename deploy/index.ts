import * as pulumi from "@pulumi/pulumi";
import * as digitalocean from "@pulumi/digitalocean";
import * as k8s from "@pulumi/kubernetes";
import { ContainerRegistry } from "@gitbeaker/node";

const name = "dokue";
const gitlabProjectId = "mdaffin/digitalocean-kube-example";
const apps = ["counter", "todo-api"];

const nginxIngressYaml =
  "https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.43.0/deploy/static/provider/do/deploy.yaml";

const cluster = new digitalocean.KubernetesCluster(name, {
  region: "lon1",
  version: "1.20.2-do.0",
  nodePool: {
    name: "pool",
    size: "s-1vcpu-2gb",
    nodeCount: 2,
  },
});

const database = new digitalocean.DatabaseCluster(name, {
  engine: "pg",
  nodeCount: 1,
  region: "lon1",
  size: "db-s-1vcpu-1gb",
  version: "12",
});

export const kubeconfig = cluster.kubeConfigs[0].rawConfig;

const clusterProvider = new k8s.Provider(name, { kubeconfig });

const nginxIngress = new k8s.yaml.ConfigFile(
  "nginxIngress",
  {
    file: nginxIngressYaml,
    transformations: [
      transform_deployment(
        "ingress-nginx-controller",
        (obj) => (obj.spec.replicas = 2)
      ),
    ],
  },
  { provider: clusterProvider }
);

new k8s.yaml.ConfigFile(
  "example",
  { file: "kube/doks.yml" },
  { provider: clusterProvider, dependsOn: [nginxIngress] }
);

const todoDatabaseSecret = new k8s.core.v1.Secret(
  "todo-api-databases",
  {
    metadata: { name: "todo-api-databases" },
    stringData: {
      databases: pulumi
        .all([
          database.host,
          database.port,
          database.database,
          database.user,
          database.password,
        ])
        .apply(([host, port, database, username, password]) =>
          JSON.stringify({
            postgres: {
              url: `postgres://${username}:${password}@${host}:${port}/${database}`,
            },
          })
        ),
    },
  },
  { provider: clusterProvider }
);

get_images().then((apps) => {
  for (const app of apps) {
    new k8s.yaml.ConfigFile(
      app.name,
      {
        file: `kube/${app.name}.yml`,
        transformations: [
          transform_deployment(app.name, (obj) => {
            obj.spec.template.spec.containers[0].image = app.latest_image;
          }),
        ],
      },
      {
        provider: clusterProvider,
        dependsOn: [nginxIngress, todoDatabaseSecret],
      }
    );
  }
});

function transform_deployment(
  name: string,
  transform: (obj: any) => void
): (obj: any) => void {
  return (obj: any) => {
    if (obj.kind === "Deployment" && obj.metadata.name === name) {
      transform(obj);
    }
  };
}

function max_by<T>(array: T[], func: (a: T, b: T) => boolean): T {
  return array.reduce((a: T, b: T) => (func(a, b) ? a : b));
}

interface Image {
    name: string,
    latest_image: string,
}

async function get_images(): Promise<Image[]> {
  interface Repo {
    name: string;
    id: number;
  }

  interface Tags {
    name: string;
    tags: { location: string[] };
  }

  interface Tag {
    name: string;
    location: string;
  }

  const inAppsGlobal = (repo: Repo) => apps.includes(repo.name);
  const getTags = async (repo: Repo) => ({
    name: repo.name,
    tags: await regApi.tags(gitlabProjectId, repo.id),
  });
  const filterLatestTag = async (repo: Promise<Tags>) => {
    const { name, tags } = await repo;
    return {
      name: name,
      latest_image: max_by([tags].flat(), (a: Tag, b: Tag) => a.name > b.name)
        .location,
    };
  };

  const regApi = new ContainerRegistry();
  const repos: Record<string, any> = await regApi.repositories(
    gitlabProjectId
  );

  const images = repos.filter(inAppsGlobal).map(getTags).map(filterLatestTag);
  return await Promise.all(images);
}
