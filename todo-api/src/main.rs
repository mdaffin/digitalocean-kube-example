use std::sync::Mutex;

use actix_web::{delete, get, post, web, App, HttpResponse, HttpServer, Responder};
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Item {
    item: String,
    #[serde(default)]
    done: bool,
}

type Items = Vec<Item>;
type ItemsData = web::Data<Mutex<Items>>;

#[get("/todo")]
async fn get_todo(items: ItemsData) -> impl Responder {
    let items: Vec<Item> = items.lock().unwrap().clone();
    web::Json(items)
}

#[post("/todo")]
async fn post_todo(items: ItemsData, item: web::Json<Item>) -> impl Responder {
    let mut items = items.lock().unwrap();
    items.push(item.into_inner());
    format!("{}", items.len() - 1)
}

#[get("/todo/{id}")]
async fn get_todo_id(items: ItemsData, id: web::Path<usize>) -> impl Responder {
    web::Json(items.lock().unwrap().get(*id).cloned())
}

#[post("/todo/{id}")]
async fn post_todo_id(
    items: ItemsData,
    id: web::Path<usize>,
    item: web::Json<Item>,
) -> impl Responder {
    if let Some(stored_item) = items.lock().unwrap().get_mut(*id) {
        *stored_item = item.into_inner();
        HttpResponse::Ok()
    } else {
        HttpResponse::NotFound()
    }
}

#[delete("/todo/{id}")]
async fn delete_todo_id(items: ItemsData, id: web::Path<usize>) -> impl Responder {
    let mut items = items.lock().unwrap();
    if *id < items.len() {
        items.remove(*id);
        HttpResponse::Ok()
    } else {
        HttpResponse::NotFound()
    }
}

#[get("/health")]
async fn health() -> impl Responder {
    HttpResponse::Ok()
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let items = web::Data::new(Mutex::new(Items::default()));
    HttpServer::new(move || {
        App::new()
            .app_data(items.clone())
            .service(get_todo)
            .service(post_todo)
            .service(get_todo_id)
            .service(post_todo_id)
            .service(delete_todo_id)
            .service(health)
    })
    .bind("127.0.0.1:8000")?
    .run()
    .await
}
